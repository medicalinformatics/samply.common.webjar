# Samply Webjars

This library offers various JavaScript and CSS projects as webjars.
You can include those webjars as a dependency to your project and use them
right away.

The Samply Webjars include the following JavaScript and CSS projects:


- JQuery 1.11.2
- Bootstrap 3.3.2
- Font Awesome 4.3.0
- Font Lato 2.0
- Select2 3.5.2
- Datatables 1.10.9


## Usage

Those are just examples. You can use `h:outputScript` just as well as `link`.

```xml
<link type="text/css" rel="stylesheet" href="#{resource['webjar:bootstrap/css/bootstrap.css']}"/>
<link type="text/css" rel="stylesheet" href="#{resource['webjar:font-lato/css/font-lato.css']}"/>
<link type="text/css" rel="stylesheet" href="#{resource['webjar:font-awesome/css/font-awesome.css']}"/>
<link type="text/css" rel="stylesheet" href="#{resource['webjar:jquery/css/jquery-ui.css']}"/>
<link type="text/css" rel="stylesheet" href="#{resource['webjar:select2/css/select2.css']}"/>
<link type="text/css" rel="stylesheet" href="#{resource['webjar:datatables/css/jquery.dataTables.min.css']}"/>
<link type="text/css" rel="stylesheet" href="#{resource['webjar:samply/css/buttons.css']}"/>

<script type="text/javascript" src="#{resource['webjar:jquery/js/jquery.min.js']}"></script>
<script type="text/javascript" src="#{resource['webjar:jquery/js/jquery-ui.min.js']}"></script>
<script type="text/javascript" src="#{resource['webjar:bootstrap/js/bootstrap.js']}"></script>
<script type="text/javascript" src="#{resource['webjar:select2/js/select2.min.js']}"></script>
<script type="text/javascript" src="#{resource['webjar:datatables/js/jquery.dataTables.min.js']}"></script>
```


## Build

Use maven to build the jar:

```
mvn clean package
```

Use it as a dependency:

```xml
<dependency>
    <groupId>de.samply.webjar</groupId>
    <artifactId>samply</artifactId>
    <version>VERSION</version>
</dependency>
```
